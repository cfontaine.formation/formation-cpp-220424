#include <iostream>

using namespace std;

int main()
{
	// condition if
	int val;
	cin >> val;
	if (val > 10) {
		cout << "la valeur est superieur a 10" << endl;
	}
	else if (val==10){
		cout << "la valeur est = a 10" << endl;
	}
	else {
		cout << "la valeur est inferieur a 10" << endl;
	}

	// Exercice: Trie de 2 Valeurs
	// Saisir 2 nombres et afficher ces nombres tri�s dans l'ordre croissant sous la forme 1.5 < 10.5
	cout << "Saisir 2 nombres: ";
	double v1, v2;
	cin >> v1 >> v2;
	if (v1 < v2) {
		cout << v1 << "<" << v2;
	}
	else {
		cout << v2 << "<=" << v1;
	}

	// Exercice: Interval
	// Saisir un nombre et dire s'il fait parti de l'intervalle - 4 (exclus)et 7 (inclus)
	 cout << "Saisir un nombre entier: ";
	int v;
	cin >> v;
	if (v > -4 && v <= 7) {
		cout << v << " fait parti de l'interval" << endl;
	}

	// Op�rateur ternaire -> utiliser pour faire une allocation conditionnelle
	int valeur;
	cin >> valeur;
	int vAbs = valeur > 0 ? valeur : -valeur;
	cout << "|" << valeur << "|=" << vAbs;

	// Condition switch
	int jour;
	const int LUNDI = 1;
	cin >> jour;
	switch (jour) {
	case LUNDI: //1
		cout << "Lundi" << endl;
		break;
	case LUNDI+5: //6
	case LUNDI+6: //7
		cout << "Week-end" << endl;
		break;
	default:
		cout << "un autre jour";
	}
	
	// Exercice: Calculatrice
	// Faire un programme calculatrice
	//	Saisir dans la console
	//	- un double
	//	- une caract�re op�rateur qui a pour valeur valide : + - * /
	//	- un double

	// Afficher:
	// - Le r�sultat de l�op�ration
	// - Une message d�erreur si l�op�rateur est incorrecte
	// - Une message d�erreur si l�on fait une division par 0
	const double EPS = 1e-9;
	double v1, v2;
	char op;
	cin >> v1 >> op >> v2;
	switch (op) {
	case '+':
		cout << v1 << " + " << v2 <<" = " << (v1 + v2) << endl;
		break;
	case '-':
		cout << v1 << " - " << v2 << " = " << (v1 - v2) << endl;
		break;
	case '*':
		cout << v1 << " * " << v2 << " = " << (v1 * v2) << endl;
		break;
	case '/':
		if (v2 == 0.0) { //(v2>-EPS && v2<EPS)
			cerr << "Division par 0" << endl;
		}
		else {
			cout << v1 << " / " << v2 << " = " << (v1 / v2) << endl;
		}
		break;
	default :
		cerr << op << " n'est pas un op�rateur" << endl;
	}

	// Boucles

	// Boucle :while
	int k = 0;
	while (k < 10) {
		cout << k << endl;
		k++;
	}

	// Boucle :do while
	k = 0;
	do {
		cout << k << endl;
		k++;
	} while (k < 10);

	// Boucle :for
	for (int i=0; i < 10; i++) {
		cout << i << endl;
	}

	// Intructions de branchement
	// break
	for (int i = 0; i < 10; i++) {
		if (i == 2) {
			break;  // -> on quitte la boucle
		}
		cout << i << endl;
	}

	// continue
	for (int i = 0; i < 10; i++) {
		if (i == 2) {
			continue;  // -> on passe � l'it�ration suivante
		}
		cout << i << endl;
	}

	// goto
	for (int i = 0; i < 10; i++) {
		for (int j = 0;  j < 5; j++) {
			if (i == 2) {
				goto EXIT_LOOP; // -> pour sortir des 2 boucles imbriqu�es
			}
			cout << i << " " << j << endl;
		}
	}
EXIT_LOOP: cout << endl; // le label doit �tre sur une "instruction"

	// boucle infinie
	//while (true) {

	//}
	//for (;;) {

	//}

	// Exercice: Table de multiplication
	// Faire un programme qui affiche la table de multiplication pour un nombre entre 1 et 9
	//	1 X 4 = 4
	//	2 X 4 = 8
	//	�
	//	9 x 4 = 36
	//	Si le nombre pass� en param�tre est en dehors de l�intervalle 1 � 9, on arr�te sinon on redemande une nouv
	for (;;) {
		int m;
		cin >> m;
		if (m < 1 || m>9) {
			break;
		}
		for (int i = 1; i < 10; i++) {
			cout << i << " x " << m << " = " << (m*i) << endl;
		}
	}


	// Exercice: Quadrillage
	// Cr�er un quadrillage dynamiquement on saisit le nombre de colonne et le nombre de ligne
	//	ex : pour 2 3

	//	[][]
	//	[][]
	//	[][]
	int col, row;
	cin >> col >> row;
	for (int r = 0; r < row; r++) {
		for (int c = 0; c < col; c++) {
			cout << "[]";
		}
		cout << endl;
	}

}
