// Fichier .h -> contient la d�claration des fonctions

#pragma once // Indique au compilateur de n'int�grer le fichier d�en-t�te qu�une seule fois, lors de la compilation d�un fichier de code source
// ou
// #ifndef MES_FONCTIONS_H
// #define MES_FONCTIONS_H

#include <string>
#include <cstdarg>
#include <initializer_list>

// d�claration d'une constante qui a �t� d�finie dans 04-fonctions.cpp. Il n' y a pas d'allocation m�moire
extern const double TEST_VAR_EXTERN;

// D�claration d'une fonction
double multiplier(double, double);

// D�claration d'une fonction sans retour -> void
void afficher(double);

// Exercice fonction maximum
int maximum(int, int);

// Exercice fonction pair
bool even(int);

// Passage de param�tre par valeur
void testParamvaleur(int);

// Passage de param�tre par r�f�rence
// Comme avec le passage de param�tres par adresse, on modifie r�ellement la variable qui est pass�e en param�tre
void testParamReference(int& v);

// Si la r�f�rence est constante, on a le m�me r�sultat qu'un passage par valeur mais sans copie 
void testParamReferenceCst(const int& v);

// Passage de param�tre par adresse
// En utilisant le passage de param�tres par adresse, on modifie r�ellement la variable qui est pass�e en param�tre
void testParamAdresse(int* v);

// Passage d'un tableau, comme param�tre d'une fonction
// On ne peut pas passer un tableau par valeur uniquement par adresse
void TestParamTableau(int tab[], int size);
// ou
void TestParamTableauPtr(int* prtTab, int size);

// On peut d�finir des valeurs par d�faut pour les param�tres. Ils doivent se trouver en fin de liste des arguments
// On place les valeurs par d�faut dans la d�claration des fonctions
void testParamDefault(int i, char c = 'a', const double& d = 2.3); // ,int *p=nullptr

// Pour les fonctions inline, on place le code de la fonction dans le fichier.h
inline double doubler(double b) {
	return 2 * b; // 2*2
}

// Surcharge de fonction
int somme(int, int);
double somme(double, double);
double somme(int, double);
int somme(int, int, int);
int somme(int*, int);
//int somme(int&, int);			// ambigue -> int somme(int, int)
// int somme(const int&, int);	// ambigue -> int somme(int, int)
double somme(double&, int);
double somme(const double&, int);

// R�cursivit�
int factorial(int n);

// Pointeur de fonction
// typedef permet de d�finir des synonymes de types -> typedef type synonyme_type;
typedef int (*ptrf)(int);

int fonction1(int a);
int fonction2(int a);
void afficheCalcul(int v, ptrf f);

// Exercice Tableau
void afficherTableau(int* tab, int size);
int maximumTableau(int* tab, int size);
double moyenneTableau(int* tab, int size);
int* saisirTableau(int& size);
int saisirTableau2(int** t);
void createEntier(int** t);

// /!\ fonctions qui retournent une r�f�rence ou un pointeur sur une variable locale
std::string* testRetour(int a);

// Fonction � nombre variable de param�tre
// h�rit� du C
double moyenne(int nbArg, ...);
// en C++ -> initializer_list
double moyenneCpp11(std::initializer_list<int> valeurs);

// Test de classe de m�morisation
int* testMemstatic();
void testMemExtern();

//#endif
