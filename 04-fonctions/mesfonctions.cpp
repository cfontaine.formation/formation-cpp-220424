#include<iostream>
#include "mesfonctions.h"  

using namespace std;

// Fichier .cpp  -> contient les D�finitions des fonctions

extern int varGlobal;	// la d�claration est d�finie dans 04-fonctions.cpp. extern -> Il n' y a pas d'allocation m�moire

// extern int varGlobal2; // comme varGlobal2 est static, on ne peut plus utiliser extern

// D�fintion d'une fonction
double multiplier(double d1, double d2) {
	return d1 * d2;
}

void afficher(double d) {
	cout << d << endl;
	// return; -> return est optionnel avec void
}

// Maximum
// �crire une fonction maximum qui prends en param�tre 2 nombres et elle retourne le maximum
// Saisir 2 nombres et afficher le maximum entre ces 2 nombres
int maximum(int a, int b) {
	//if (a < b) {
	//    return b;
	//}
	//else {
	//    return a;
	//}

	// ou

	return a < b ? b : a;
}

// Fonction Paire
// �crire une fonction even qui prend un entier en param�tre
// Elle retourne vrai si il est paire
bool even(int v) {
	//if (v % 2 == 0) {
	//    return true;
	//}
	//else {
	//    return false;
	//}

	// ou
	//return (v & 0x1) == 0;

	// ou
	return v % 2 == 0;
}

// Passage de param�tres par valeur
// La valeur du param�tre est copi�e et une modification sur la copie n�entra�ne pas la modification de l�original
void testParamvaleur(int v)
{
	cout << "v" << v << endl;
	v++;
	cout << "v" << v << endl;
}

// Passage de param�tre par r�f�rence
// Comme avec le passage de param�tres par adresse, on modifie r�ellement la variable qui est pass�e en param�tre
void testParamReference(int& v)
{
	cout << "v" << v << endl;
	v++;
	cout << "v" << v << endl;
}

// avec une r�f�rence constante, la valeur du parm�tre ne peut pas 
void testParamReferenceCst(const int& v)
{
	cout << "v" << v << endl;
	//  v++; // on ne peut modifier la r�f�rence
	cout << "v" << v << endl;
}

// En utilisant le passage de param�tres par adresse, on modifie r�ellement la variable qui est pass�e en param�tre
void testParamAdresse(int* v)
{
	cout << v << " " << *v << endl;
	(*v)++;
	cout << v << " " << *v << endl;
}

void TestParamTableau(int tab[], int size)
{
	for (int i = 0; i < size; i++) {
		cout << tab[i] << " ";
	}
	cout << endl;
}

void TestParamTableauPtr(int* prtTab, int size)
{
	for (int i = 0; i < size; i++) {
		cout << prtTab[i] << " ";
	}
	cout << endl;
}

// Les param�tres par d�faut sont uniquement d�fini dans la d�claration de la fonction (.h)
void testParamDefault(int i, char c, const double& d)
{
	cout << i << " " << c << " " << d << endl;
}

// Surcharge de fonction
// Plusieurs fonctions peuvent avoir le m�me nom
// Ces arguments doivent �tre diff�rents en nombre ou/et en type pour qu'il n'y ai pas d'ambiguit� pour le compilateur
int somme(int a, int b) {
	cout << "2 entiers" << endl;
	return a + b;
}

double somme(double a, double b)
{
	cout << "2 double" << endl;
	return a + b;
}

double somme(int a, double b)
{
	cout << "un entier , un double" << endl;
	return a + b;
}

int somme(int a, int b, int c)
{
	cout << "3 entiers" << endl;
	return a + b + c;
}

int somme(int* a, int b) {
	cout << "un pointeur et un entier" << endl;
	return *a + b;
}

//int somme(int& a, int b)	 // erreur -> ambigue corespond � somme(int a, int b)
//{
//    cout << "une r�f�rence et un entier" << endl;
//    return a + b;
//}

//int somme(const int& a, int b) {	// erreur -> ambigue corespond � somme(int a, int b)
//    cout << "une r�f�rence  cstet un entier" << endl;
//    return a + b;
//}

double somme(double& a, int b)
{
	return a + b;
}

double somme(const double& a, int b)
{
	return  a + b;
}

// Fonction recursive
int factorial(int n) // factoriel= 1* 2* � n
{
	if (n <= 1)// condition de sortie
	{
		return 1;
	}
	else
	{
		return factorial(n - 1) * n;
	}
}

// Pointeur de fonction
int fonction1(int a)
{
	return a * 3;
}

int fonction2(int a)
{
	return a / 2;
}

void afficheCalcul(int v, ptrf f)
{
	cout << f(v) << endl;
}

// void afficherTableau(int tab[], int size)
// ou
void afficherTableau(int* tab, int size)
{
	cout << "[ ";
	for (int i = 0; i < size; i++) {
		cout << tab[i] << " ";
	}
	cout << "]" << endl;
}



int maximumTableau(int* tab, int size)
{
	int max = INT_MIN;
	for (int i = 0; i < size; i++) {
		if (tab[i] > max) {
			max = tab[i];
		}
	}
	return max;
}

double moyenneTableau(int* tab, int size)
{
	double somme = 0.0;
	for (int i = 0; i < size; i++) {
		somme += tab[i];
	}
	return somme / size;
}

int* saisirTableau(int& size)
{
	cin >> size;
	int* tabDyn = nullptr;
	if (size > 0) {
		tabDyn = new int[size];
		for (int i = 0; i < size; i++) {
			cout << "t[" << i << "]=";
			cin >> tabDyn[i];
		}
	}
	return tabDyn;
}

// Pointeur de pointeur
int saisirTableau2(int** t)
{
	int size;
	cin >> size;
	*t = nullptr;
	if (size > 0) {
		*t = new int[size];
		for (int i = 0; i < size; i++) {
			cout << "t[" << i << "]=";
			cin >> (*t)[i];
		}
	}
	return size;
}

void createEntier(int** t)
{
	cout << *t << endl;
	*t = new int;

	cout << t << endl;
}

// /!\ une fonction ne doit pas retourner une r�f�rence ou un pointeur sur une variable locale � une fonction
string* testRetour(int n)
{
	// string str = string(n, 'a');	// variable locale -> erreur
	// si on retourne un pointeur ou une r�f�rence il faut que l'allouer dynamiquement
	string* ptrStr = new string(n, 'a');
	return ptrStr;
}

// Nombre d'arguments variable
// en C et C++98
double moyenne(int nbArg, ...)
{
	va_list vl;
	va_start(vl, nbArg);	// initialise la liste des arguments variables. On passe en param�tre le nom du dernier param�tre nomm�
	double somme = 0.0;
	for (int i = 0; i < nbArg; i++) {
		somme = somme + va_arg(vl, int);	// va_arg permet de recup�rer les valeurs des arguments dans le m�me ordre que celui transmis � la fonction
	}										// On passe en param�tre le type de l�argument
	va_end(vl);
	return nbArg == 0 ? 0.0 : somme / nbArg;

}

// en C++11 => initializer_list
double moyenneCpp11(std::initializer_list<int> valeurs)
{
	double somme = 0.0;
	for (auto val : valeurs) {
		somme += val;
	}
	return valeurs.size() == 0 ? 0 : somme / valeurs.size();
}

// Classe de m�morisation static
int* testMemstatic()
{
	// Entre 2 ex�cutions i conserve sa valeur
	static int i = 100;	// par d�faut initialis� � 0	
	i++;				// i n'est visible que dans la fonction
	cout << i << endl;
	return &i;
}

// Classe de m�morisation extern
void testMemExtern()
{
	cout << varGlobal << " " << TEST_VAR_EXTERN << " ";//  << varGlobal2 << endl;

	varGlobal = 123;
}
