#include <iostream>
#include "mesfonctions.h"
#include "MesStructures.h"

using namespace std;

// Déclaration des fonctions => déplacer dans le fichier .h d'en-têtes
/*  double multiplier(double, double);
	void afficher(double);
	int maximum(int, int);
	bool even(int);*/

int varGlobal = 10;
const double TEST_VAR_EXTERN = 1.23;

// avec static la variable global n'est visible que dans ce fichier
static int varGlobal2 = 10;

// Enumeration
// C++ 98
enum Motorisation { ESSENCE = 95, DIESEL = 40, GPL = 30, ELECTRIQUE = 180, ETHANOL = 10 };

enum Direction { NORD = 90, SUD = 270, EST = 0, OUEST = 180 };

// En C++11 -> enum class 
enum class Emballage { PLASTIQUE, CARTON, PAPIER, SANS };

// main -> point d'entré du programme
// argc -> nombre d'élément du tableau, au minimum 1
// argv[] -> tableau de chaine de caractères qui contient les paramètres en ligne de commande, au minimum le nom du programme 
int main(int argc, char* argv[])
{
	// Appel de methode
	double r = multiplier(2.3, 4.5);
	cout << r << endl;
	cout << multiplier(2.0, 3.0) << endl;

	// Appel de methode (sans retour)
	afficher(r);

	// Exercice Fonction maximum
	int v1, v2;
	cin >> v1 >> v2;
	cout << maximum(v1, v2) << endl;

	// Exercice Fonction paire
	cout << even(3) << endl; // 0
	cout << even(4) << endl; // 1

	// Paramètre passé par valeur (par défaut)
	// C'est une copie de la valeur du paramètre qui est transmise à la méthode
	int val = 10;
	testParamvaleur(val);
	cout << "val=" << val << endl;
	testParamvaleur(4);

	// Passage de paramètre par référence (uniquement en C++)
	testParamReference(val);
	cout << "reference val=" << val << endl;
	//  testParamReference(4); // uniquement des variables

	// Passage de paramètre par référence constante
	testParamReferenceCst(val); // Passage de paramètre par référence constante
	cout << val << endl;
	testParamReferenceCst(4); // Avec une référence constante, on peut aussi passer des littérals

	// Paramètre passé par adresse
	// La valeur de la variable passée en paramètre est modifiée, si elle est modifiée dans la méthode
	testParamAdresse(&val); // on passe en paramètre l'adresse de la variable val
	cout << val << endl;

	// Les tableaux peuvent être passé uniquement par adresse
	int t[] = { 2,6,3,8 };
	TestParamTableau(t, 4);  // le nom du tableau correspond à un pointeur sur le premier élément du tableau
	TestParamTableauPtr(t, 4);

	// Paramètre par défaut (uniquement pour les paramètres passés par valeur et référence constante)
	testParamDefault(3);        // 3 'a' 2.3
	testParamDefault(4, 'z');    // 4 'z' 2.3
	testParamDefault(8, 'e', 3.14); // 8 'e' 3.14

	// Fonction inline (équivalant au macro en C mais avec une vérification des types)
	// le compilateur peut remplacer l'appel de fonction par le contenu de la fonction aumont de la compilation
	// à utiliser qu'avec des fonctions qui ne font que quelques lignes -> duplication du code
	double d = doubler(2.0);  // 2.0*2 -> 4.0
	cout << d << endl;

	// Surcharge de fonction
	// correspondance exacte
	cout << somme(1, 2) << endl;
	cout << somme(1.3, 5.6) << endl;
	cout << somme(1.3, 5) << endl;
	cout << somme(1, 2, 7) << endl;
	int vs = 4;
	cout << somme(&vs, 4) << endl;
	cout << somme(vs, 4) << endl;

	// Pas correpondence direct des paramètres
	// le compilateur va faire des conversions implicitent des paramètres pour trouver une fonction qui correspond 
	cout << somme(1.2, 3.6F) << endl;   // Convertion du float en double
	cout << somme('a', 'A') << endl;    // Convertion des caractères en entier
	cout << somme(true, false) << endl; // Convertion des booleans en entier

	// Les arguments double& et const double& sont considérés comme différents
	double vd = 3.4;
	cout << somme(vd, 4) << endl;
	cout << somme(4.5, 4) << endl;

	// Fonction récurssive
	int fac = factorial(3);
	cout << fac << endl;

	// Paramètre de la fonction main
	for (int i = 0; i < argc; i++) {
		cout << argv[i] << endl;
	}

	// Pointeur de fonction
	ptrf pf = fonction1;
	cout << pf(2) << endl;
	pf = fonction2;
	cout << pf(2) << endl;

	afficheCalcul(2, fonction1);
	afficheCalcul(2, fonction2);
	afficheCalcul(2, factorial);

	// Exercice tableau    
	int size;
	int* tab = saisirTableau(size);
	afficherTableau(tab, size);
	cout << "Maximum=" << maximumTableau(tab, size) << " Moyenne=" << moyenneTableau(tab, size) << endl;
	delete[] tab;

	// Pointeur de pointeur
	int* ti = nullptr;
	cout << "adresse de ti" << &ti << endl;
	createEntier(&ti);
	cout << "adresse de ti" << &ti << endl;
	cout << "ti=" << ti << endl;
	delete ti;

	size = saisirTableau2(&tab);
	afficherTableau(tab, size);
	delete[] tab;

	// /!\ une fonction ne doit pas retourner une référence ou un pointeur sur une variable locale à une fonction
	string* str = testRetour(4);
	cout << *str << endl;
	delete str;

	// Fonction à nombre variable de paramètre  
	// Hérité du C => Problème pas de vérification de type
	cout << moyenne(4, 10, 12, 9, 14) << endl;
	cout << moyenne(2, 10, 12) << endl;

	// C++ 11 => initializer_list
	cout << moyenneCpp11({ 10, 12, 9, 14 }) << endl;

	// Classe de mémorisation static
	int* ptrI = testMemstatic();
	(*ptrI)++;
	testMemstatic();
	testMemstatic();
	testMemstatic();

	cout << varGlobal << endl;

	// Classe de mémorisation extern
	cout << TEST_VAR_EXTERN << endl;
	testMemExtern();

	// Enumération
	// en c++
	// motorisation mt = DIESEL;
	Motorisation mt = Motorisation::DIESEL;

	if (mt == Motorisation::DIESEL) {
		cout << "Diesel" << endl;
	}

	// Convertion implicite énumération -> int
	int imt = mt;
	cout << imt << endl;

	// Convertion explicite  int -> énumération (à éviter)
	int valMt = 120;
	imt = (Motorisation)valMt;
	cout << imt << endl;

	// en C++98, pas de vérification de type enum 
	Direction dir = SUD;
	Motorisation mt2 = ELECTRIQUE;
	if (dir == mt2) {
		cout << "==" << endl;
	}

	// en C++ 11
	// emballage emb = CARTON; // erreur
	Emballage emb = Emballage::CARTON;

	// en C+11 il n'y plus convertion implicte
	// int iEmb = emb;
	int iEmb = static_cast<int>(emb);	// convertion explicite
	cout << iEmb << endl;

	// enum class -> il ya une vérificcation du type
	Direction dir1 = Direction::SUD;
	//if (m == dir1) {
	//	cout << "==" << endl;
	//}

	// On peut utiliser une enumération avec un switch
	switch (emb) {
	case Emballage::CARTON:
		cout << "carton" << endl;
		break;
	default:
		cout << "autre" << endl;
	}

	// Structure
	struct Contact c1; // en C
	Contact c2; // en C++

	// accèder à un champs l'opérateur .
	c2.prenom = "John";
	c2.nom = "Doe";
	c2.email = "jdoe@dawan.fr";
	c2.age = 45;
	c2.afficher(); // en C++ on p

	// Déclaration et initialisation d'une structure
	Contact c3 = { "Jane","Doe","jane.doe@dawan.com",34 };

	// Affectation d'une structure
	// intialisation des champs de c4 avec les valeurs des champs de c3 
	Contact c4 = c3;

	// On test l'égalité de 2 strucutures -> champs par champs
	bool test = c1.age == c2.age && c1.prenom == c2.prenom;

	// Alouer dynamiquement une structure
	Contact* ptrC = new Contact;
	(*ptrC).prenom = "Jim";
	ptrC->nom = "Profit";	// Opérateur fléche
	ptrC->email = "jp@dawan.com";
	ptrC->age = 30;
	ptrC->afficher();
	//...
	delete ptrC;

	// structure constante
	const Contact cstC = { "Alan","Smithee","asmithee@dawan.fr",56 };
	cout << cstC.prenom << endl;
	// cstC.prenom = "Sony";
	cstC.age = 57; // mutable 


	// Chaine de caractère en C
	// en C, un chaine de caractère est un tableau de caractère qui est terminé par un caractère terminateur '\0'
	// Une chaine constante -> const char*
	const char* strC1 = "Hello world";
	cout << strC1 << endl;
	cout << strC1[0] << endl;
	// strC1[0] = 'h'; // -> erreur

	// chaine de caractère -> tableau de caractère
	char strC2[] = "Hello";
	strC2[0] = 'h';
	cout << strC2 << endl;
	// Nombre de caractères de la chaine +1 pour le terminateur \0
	cout << sizeof(strC2) << endl; // 5 + 1

	// Chaine de caractère en C++ -> string
	string strCpp = "hello";
	string strCpp2 = string(10, 'z');
	cout << strCpp << " " << strCpp[0] << endl;
	cout << strCpp.c_str() << endl;

	// Allouer dynamiquement 
	string* str3 = new string("azerty");
	cout << *str3 << endl;
	delete str3;
}

// Définition des fonctions -> déplacer dans le fichier .cpp
//double multiplier(double d1, double d2) {
//    return d1 * d2;
//}
//
//void afficher(double d) {
//    cout << d << endl;
//    // return;
//}
//
//int maximum(int a, int b) {
//    //if (a < b) {
//    //    return b;
//    //}
//    //else {
//    //    return a;
//    //}
//    return a < b ? b : a;
//}
//
//bool even(int v) {
//    //if (v % 2 == 0) {
//    //    return true;
//    //}
//    //else {
//    //    return false;
//    //}
//    // ou
// //   return v % 2 == 0;
//    // ou
//    return (v & 0x1) == 0;
//}