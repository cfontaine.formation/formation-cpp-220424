#pragma once
#include "CompteBancaire.h"

class CompteEpargne : public CompteBancaire
{
	double taux;

public:
	CompteEpargne(double taux, std::string titulaire, double solde) : CompteBancaire(titulaire, solde) {
		this->taux = taux;
	}

	double getTaux() const {
		return taux;
	}

	void setTaux(double taux) {
		this->taux = taux;
	}

	void calculInterets();
	void afficher() const; // red�finition de la m�thode afficher de la classe m�re

};

