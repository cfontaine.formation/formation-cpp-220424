#include "VoiturePrioritaire.h"

using namespace std;

void VoiturePrioritaire::afficher() const
{
	Voiture::afficher(); // -> appel de la m�thode afficher de la classe m�re
	cout << "gyro =" << (gyro ? "allume" : "eteint") << endl;
}
