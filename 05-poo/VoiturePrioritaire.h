#pragma once
#include "Voiture.h"
#include <iostream>
class VoiturePrioritaire : public Voiture // VoiturePrioritaire h�rite de Voiture
{
	bool gyro;

public:
	VoiturePrioritaire() : gyro(false) { // appel implicite du constructeur par d�faut de la classe m�re
		std::cout << "Constructeur defaut VoiturePrioritaire" << std::endl;
	}

	VoiturePrioritaire(std::string modele, std::string couleur, std::string plaqueIm, bool gyro)
		: Voiture(modele, couleur, plaqueIm) { // appel explicite du constructeur 3 param�tres de la classe m�re
		this->gyro = gyro;
	}
	bool getGyro() const {
		return gyro;
	}

	void allumerGyro() {
		vitesse *= 2; // comme l'attribut vitesse est protected, on peut y acc�der dans la classe enfant
		// couleur="bleu" // couleur est priv�e -> une voiture prioritaire a une couleur mais on ne peut pas y acc�der directement.
		gyro = true;
	}

	void eteindreGyro() {
		gyro = false;
	}

	void afficher() const override; // avec C++ 11 permet de d�finir  que la m�thode red�finit une m�thode d�une classe m�re.
};

