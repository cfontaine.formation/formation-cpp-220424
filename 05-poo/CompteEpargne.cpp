#include "CompteEpargne.h"
#include <iostream>

void CompteEpargne::calculInterets()
{
	solde *= (1 + taux / 100);
}

void CompteEpargne::afficher() const
{
	CompteBancaire::afficher();
	std::cout << taux << std::endl;
}
