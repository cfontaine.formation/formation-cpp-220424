#pragma once
#include <string>
#include <iostream>
#include "Personne.h"
#include "Moteur.h"

class Voiture
{
	// class -> par d�faut private
//private: -> uniquement acc�ssible � l'int�rieur de la classe

	// Encapsulation -> toutes les attributs sont priv�s

	// Variable d'instance
	std::string modele = "toyota yaris"; // en C++ 11 On peut initialiser les variable d'instance
	std::string couleur;
	std::string plaqueIm;
	int compteurKm;

protected: // -> acc�ssible � l'int�rieur de la classe et par les classes enfants
	int vitesse = 0;

private:
	// Variable de classe
	static int nbVoiture;

	// Agr�gation
	Personne* proprietaire;

	// Composition
	// Moteur moteur;
	// ou
	Moteur* moteur;


public: // -> acc�ssible � l'exterieur de la classe

	// Constructeur

	// Constructeur par d�faut (sans param�tres)
	// Voiture();

	// c++ 11 default -> va permettre de cr�er un constructeur par d�faut
	//Voiture() = default;

	Voiture(std::string modele, std::string couleur, std::string plaqueIm);

	// Liste d'initialisation
	Voiture(std::string modele, std::string couleur, std::string plaque, int vitesse) : modele(modele), couleur(couleur),
		plaqueIm(plaque), vitesse(vitesse), compteurKm(200), proprietaire(nullptr),moteur(new Moteur()) {
		nbVoiture++;
		std::cout << "==> Constructeur Voiture 4 param�tres" << std::endl;
	}

	Voiture(std::string modele, std::string couleur, std::string plaque, Personne& personne)
		: modele(modele), couleur(couleur), plaqueIm(plaque), vitesse(0), compteurKm(200), proprietaire(&personne), moteur(new Moteur()) {
		nbVoiture++;
	}

	Voiture(std::string modele, std::string couleur, std::string plaqueIm, int puissanceCh, double consommation100) : modele(modele), couleur(couleur),
		plaqueIm(plaqueIm), vitesse(0), compteurKm(200), proprietaire(nullptr), moteur(new Moteur(puissanceCh, consommation100)) {
		nbVoiture++;
	}

	// Constructeur par copie (ou recopie) 
	// Par d�faut, le constructeur par copie  copie les valeurs des attributs
	// Si cela ne nous convient pas, (surtout si l'on a des attributs allou�s dynamiquement),il faudra l'�crire explictement

	Voiture(const Voiture& v);

	// Interdire le constructeur par copie
	// 
	// - 1 d�clarer le constructeur par copie et on ne cr�e pas de d�finition 
	// Voiture(const Voiture& v);

	// - 2 rendre le constructeur par copie priv�e
	//private: 
	//	Voiture(const Voiture& v);
	//public:

	// -3 en C++ 11 -> delete
	// Voiture(const Voiture& v) = delete;

	// Constructeur d�l�gu�  (uniquement C++11) -> On peut chainer les constructeurs 
	Voiture() : Voiture("Fiat Punto", "jaune", "zi-3487-rt", 30) { // ici, le constructeur par d�faut appel le constructeur 4 param�tres, avant d'ex�cuter son bloc de code 
		std::cout << "Constructeur d�l�gu� par d�faut" << std::endl;
	}

	// Destructeur -> appeler avant la destruction de l'objet
	// on le d�finie, si l'on des ressources � lib�rer
	~Voiture();

	// Encapsulation -> toutes les attributs sont priv�s, on y acc�de par l'interm�diare de m�thodes (getter/setter)
	
	// Getter/Setter
	// Les m�thodes qui sont d�finie dans .h est implicitement inline
	// const-> Un m�thode peut �tre constante si elle ne modifie pas l'�tat de l'objet
	// Les m�thodes constantes sont les seules que l'on peut utiliser, si l'objet est constant

	std::string getModele() const {
		return modele;
	}

	std::string getCouleur() const {
		return couleur;
	}

	void setCouleur(std::string couleur) {
		this->couleur = couleur;	//  this : permet de lever l'ambiguit� entre une variable d'instance et le param�tre
	}

	std::string getPlaqueIm() const {
		return plaqueIm;
	}

	void setPlaqueIm(std::string plaqueIm) {
		this->plaqueIm = plaqueIm;
	}

	int getVitesse() const {
		return vitesse;
	}

	void setVitesse(int vitesse) {
		if (vitesse > 0) {
			this->vitesse = vitesse;
		}
	}

	int getCompteurKm() const {
		return compteurKm;
	}

	static int getNbVoiture() { // /!\ une m�thode static ne peut pas �tre const
		return nbVoiture;
	}

	Personne& getProprietaire() {
		return *proprietaire;
	}

	void setProprietaire(Personne* proprietaire) {
		this->proprietaire = proprietaire;
	}

	// M�thode d'instance
	void accelerer(int vAcc);
	void freiner(int vFrn);
	void arreter();
	bool estArreter() const;
	virtual void afficher() const;

	// M�thode de classe
	static void testMethodeClasse();
	static bool egaliteVitesse(Voiture& va, Voiture& vb);
};

