#include <iostream>
#include "Voiture.h"
#include "CompteBancaire.h"
#include "VoiturePrioritaire.h"
#include "CompteEpargne.h"

using namespace std;

int main()
{
	// Accès à une variable de classe
	// cout << "variable de classe=" << Voiture::nbVoiture << endl;
	cout << "variable de classe=" << Voiture::getNbVoiture() << endl; // encapsulation

	// Appel méthode de classe
	Voiture::testMethodeClasse();
	
	// Instanciation statique ou automatique
	Voiture v1;
	cout << "variable de classe=" << Voiture::getNbVoiture() << endl;

	// Appel d'une méthode d'instance
	v1.setVitesse(10);// v1.vitesse = 10; // encapsulation -> on n'a plus accès aux variables d'instance directement, on y accède par l'intermédiare de méthode get/set
	cout << v1.getVitesse() << endl; //v1.vitesse << endl; 
	v1.accelerer(40);
	v1.afficher();
	v1.freiner(20);
	v1.afficher();
	cout << v1.estArreter() << endl;
	v1.arreter();
	cout << v1.estArreter() << endl;

	// Instanciation dynamique -> new
	Voiture* v2 = new Voiture;
	cout << "variable de classe=" << Voiture::getNbVoiture() << endl;
	//(*v2).vitesse = 30;
	v2->setVitesse(30); // avec npointeur, on utilise l'opérateur ->
	v2->afficher();

	delete v2; // -> destruction de l'objet instancié dynamiquement

	Voiture* v3 = new Voiture("Ford Puma", "bleu","er-7853-fr");
	cout << "variable de classe=" << Voiture::getNbVoiture() << endl;
	v3->afficher();

	Voiture v4("Ford Puma", "noir", "er-7853-fr");
	v4.afficher();

	// Constructeur par copie
	Voiture v5(v1); // ou Voiture v5 = v1;
	v5.afficher();

	// Méthode de classe
	cout << Voiture::egaliteVitesse(v1, v4) << endl;

	// Object constant
	const Voiture v6("Peugot 206", "Rouge", "er-3456-Fv");
	v6.afficher();
	v6.estArreter(); // on peut utiliser uniquement les méthodes const avec un objet constantv (pas de modification de l'état)
	//v6.freiner();	 // erreur -> la méthode modifie l'état de l'objet 

	// Agrégation
	Personne* per1 = new Personne("John", "Doe");
	Voiture* v7 = new Voiture("bmw z1", "blanc", "sd-2765-cv", *per1);
	v7->afficher();

	// Héritage
	VoiturePrioritaire vp1;
	vp1.afficher();
	cout << vp1.getGyro() << endl;
	vp1.accelerer(20);
	vp1.afficher();

	VoiturePrioritaire* vp2 = new VoiturePrioritaire("subaru", "rouge", "er-9110-fr", true);
	vp2->afficher();

	// Exercice: Compte Bancaire
	CompteBancaire cb1("John Doe", 100.0);

	//cb1.titulaire = "John Doe";
	//cb1.iban = "fr5962-000000";
	//cb1.solde = 100.0;

	cout << cb1.getSolde() << endl;
	cb1.crediter(200.0);
	cb1.afficher();
	cb1.debiter(150.0);
	cout << cb1.estPositif() << endl;
	cb1.afficher();

	// Exercice Héritage: Compte Epargne
	CompteEpargne* ce = new CompteEpargne(1.5, "Elliot Alderson", 1000.0);
	ce->afficher();
	ce->calculInterets();
	ce->afficher();

	delete v3;
	delete v7;
	delete vp2;
	delete per1;
	delete ce;
}