#include "Voiture.h"
#include <iostream>

using namespace std;


// Dans le fichier cpp, On ajoute le nom de la classe suvie de l'op�rateur de port� ::
// pour indiquer que la m�thode, le constructeur... appartient � la classe

// Initialisation de la variable de classe (obligatoirement d�finie dans le cpp)
int Voiture::nbVoiture = 0;


//Voiture::Voiture()
//{
//	modele = "Honda civic";
//	couleur = "gris";
//	plaqueIm = "fr-5435-ax";
//	vitesse = 0;
//	compteurKm = 10;
//}

Voiture::Voiture(std::string modele, std::string couleur, std::string plaqueIm)
{
	this->modele = modele;
	this->couleur = couleur;
	this->plaqueIm = plaqueIm;
	this->vitesse = 0;
	this->compteurKm = 0;
	this->proprietaire = nullptr;
	this->moteur = new Moteur();
	nbVoiture++;
	cout << "==> Constructeur Voiture 3 param�tres" << endl;

}

Voiture::~Voiture()
{
	cout << "==> Destructeur" << endl;
	delete moteur;
}

Voiture::Voiture(const Voiture& v)
{
	cout << "==> Constructeur par copie Voiture" << endl;
	modele = v.modele;
	couleur = v.couleur;
	plaqueIm = v.plaqueIm;
	vitesse = v.vitesse;
	compteurKm = v.compteurKm;
	proprietaire = v.proprietaire;
	moteur = new Moteur(*v.moteur);
	nbVoiture++;
}


// M�thode d'instance
void Voiture::accelerer(int vAcc)
{
	if (vAcc > 0) {
		vitesse += vAcc;
	}
}

void Voiture::freiner(int vFrn)
{
	if (vFrn > 0) {
		vitesse -= vFrn;
	}
}

void Voiture::arreter()
{
	vitesse = 0;
}

bool Voiture::estArreter() const
{
	return vitesse == 0;
}

void Voiture::afficher() const
{
	cout << modele << " " << couleur << " " << plaqueIm << " " << vitesse << " " << compteurKm << endl;
	if (proprietaire != nullptr) { // on test si le pointeur est diff�rent de null
		proprietaire->afficher();  // pour �viter d'appeler une m�thode sur un pointeur null -> exception
	}
	moteur->afficher();
}


// M�thode de classe
void Voiture::testMethodeClasse()
{
	cout << "Methode de classe" << endl;
	// Dans une m�thode de classe:
	//vitesse = 10; // on a pas acc�s aux variables d'instance
	//freiner(10);	// on a pas acc�s aux m�thodes d'instance

	cout << nbVoiture << endl;	// on a acc�s aux variables de classe
	// et aussi aux autres m�thodes de classe
}

bool Voiture::egaliteVitesse(Voiture& va, Voiture& vb)
{
	// Dans un m�thode de classe, on peut uniquement acc�der au variable et m�thode d'instance
	// par l'interm�diaire d'objet pass� en param�tre
	return va.vitesse == vb.vitesse;
}
