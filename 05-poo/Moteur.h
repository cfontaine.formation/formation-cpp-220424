#pragma once
class Moteur
{
	int puissanceCh;
	double consomation100;

public:
	Moteur(int puissanceCh = 100, double consomation100 = 6.2) :puissanceCh(puissanceCh), consomation100(consomation100) {

	}

	int getPuissanceCh() const {
		return puissanceCh;
	}

	double getConsomation100() const {
		return consomation100;
	}

	void afficher() const;

};

