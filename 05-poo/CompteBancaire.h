#pragma once
#include<string>

class CompteBancaire
{
	std::string titulaire;
	std::string iban;

	static int cptCompte;
protected:
	double solde;


public:
	CompteBancaire(std::string titulaire) : titulaire(titulaire), solde(0.0), iban("") {
		ibanGenerator(); // pour �viter la duplication de code, on peut placer la g�n�ration de l'iban dans une m�thode priv�
		/* cptCompte++;
		iban = "fr-5962-0000-" + std::to_string(cptCompte); */
	}


	CompteBancaire(std::string titulaire, double solde) : titulaire(titulaire), solde(solde), iban("") {
		ibanGenerator();
		/*cptCompte++;
		iban = "fr-5962-0000-" + std::to_string(cptCompte);*/
	}

	// ---------------------------------------------------------------------
	// idem en utilsant les Constructeur d�l�gu� 
	// CompteBancaire(std::string titulaire) : CompteBancaire(titulaire,0.0) {
	//	ibanGenerator();	
	// }

	// CompteBancaire(std::string titulaire, double solde) :CompteBancaire(titulaire) {
	//	this->solde = solde;
	//	ibanGenerator();
	// }
	// ---------------------------------------------------------------------

	// ---------------------------------------------------------------------
	// idem en utilsant les valeurs par d�faut pour les param�tres du constructeur
	// CompteBancaire(std::string titulaire, double solde = 0) : titulaire(titulaire), solde(solde) {
	//  	ibanGenerator();
	//	}
	// --------------------------------------------------------------------- 

	// On interdit le constructeur par copie
	CompteBancaire(const CompteBancaire& cb) = delete;

	std::string getTitulaire() const {
		return titulaire;
	}

	void setTituaire(std::string titulaire) {
		this->titulaire = titulaire;
	}

	double getSolde() const {
		return solde;
	}

	std::string getIban() const {
		return iban;
	}

	void crediter(double valeur);
	void debiter(double valeur);
	bool estPositif() const;
	virtual void afficher() const;


private:
	void ibanGenerator();

};

