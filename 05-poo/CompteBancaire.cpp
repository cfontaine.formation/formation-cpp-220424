#include "CompteBancaire.h"
#include <iostream>

using namespace std;

int CompteBancaire::cptCompte = 0;

void CompteBancaire::crediter(double valeur)
{
	if (valeur > 0) {
		solde += valeur;
	}
}

void CompteBancaire::debiter(double valeur)
{
	if (valeur > 0) {
		solde -= valeur;
	}
}

bool CompteBancaire::estPositif() const
{
	return solde >= 0;
}

void CompteBancaire::afficher() const
{
	cout << "__________________________" << endl;
	cout << "Solde=" << solde << endl;
	cout << "Titulaire=" << titulaire << endl;
	cout << "Iban=" << iban << endl;
	cout << "__________________________" << endl;
}

void CompteBancaire::ibanGenerator() {
	cptCompte++;
	iban = "fr-5962-0000-" + std::to_string(cptCompte);
}