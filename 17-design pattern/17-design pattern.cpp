#include <iostream>
#include "Singleton.h"

using namespace std;

int main()
{
	Singleton* s = Singleton::getInstance();
	s->setValeur(100);
	std::cout << s->getValeur() << std::endl;
	Singleton* s1 = Singleton::getInstance();
	std::cout << s1->getValeur() << std::endl;
	return 0;
}
