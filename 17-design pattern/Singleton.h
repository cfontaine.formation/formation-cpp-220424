#pragma once
class Singleton
{
	static Singleton* instance;

	Singleton() :valeur(0)
	{

	}

	int valeur;
public:

	static Singleton* getInstance();

	int getValeur() const
	{
		return valeur;
	}

	void setValeur(int valeur)
	{
		this->valeur = valeur;
	}

};


