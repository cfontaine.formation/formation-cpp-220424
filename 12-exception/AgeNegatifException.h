#pragma once
#include <exception>
#include <string>

// On peut d�finir ses propres exceptions en h�ritant de la classe exception ou d�une de ses classes fille
class AgeNegatifException : public std::exception
{
	std::string message;
	
public: 
	AgeNegatifException(int age) : message("l'age est negatif " + std::to_string(age)) {

	}

	// La m�thode what() sert � renvoyer le message d'erreur
	const char* what() const;
};

