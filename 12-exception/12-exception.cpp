#include <iostream>
#include "AgeNegatifException.h"

using namespace std;

void traitementAge(int age);
void etatCivil(int age);
void afficher() noexcept;

int main()
{
	int age;
	cin >> age;
	try {   // portion de code o� une exception peut-�tre attrap�, dans ce cas le programme est d�tourn� vers le catch qui correspont au type de l'exception
		etatCivil(age);
		cout << "Suite du programme" << endl;
	}
	catch (int e) // attrape une exception de type int
	{
		cerr << "exception int" << e << endl;
		// � la sortie du catch,l'exception est trait�, le programme reprend son execution normal
	}
	catch (const AgeNegatifException& e)   // attrape une exception  AgeNegatifException 
	{
		cerr << e.what() << endl;
	}
	catch (char) {  // attrape une exception de type char
		cerr << "exception char" << endl;
	}
	catch (...) {   // Attraper toutes les exceptions
		cerr << " les autre exception" << endl;
	}
	cout << "Fin de programme" << endl;
}

void etatCivil(int age)
{
	cout << "debut traitement �tat Civil" << endl;
	try {
		traitementAge(age);
	}
	catch (AgeNegatifException e) {
		cout << "traitement local de l'exception" << endl;
		throw;  // Relance de l'exception pour qu'elle soit traiter � diff�rent "niveau" de l'application 
	}
	cout << "fin traitement �tat Civil" << endl;
}

// Une exception si elle n'est pas traiter, remonte la pile d'appel des m�thodes, si elle n'est pas trait� dans le main -> le programme s'arr�te
void traitementAge(int age) //throw(int,char) en C++98
{
	cout << "debut traitement age" << endl;
	if (age < 0) {
		// throw age;   // lance une exception de type entier
		throw AgeNegatifException(age);
	}
	else if (age == 0) {
		throw 'n';      // lance une exception de type char
	}
	cout << "fin traitement age" << endl;
}

// Si une fonction ne lance aucune exception
void afficher() noexcept // noexcept en C++11 ou en C++98 throw() 
{
	cout << "pas d'exception" << endl;
}
