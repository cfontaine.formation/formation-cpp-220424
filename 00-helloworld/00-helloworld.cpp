#include <iostream>

using namespace std;

/*
	Commentaire
	sur plusieurs
	lignes
*/

// main -> Point d'entrée du programme
int main() 
{
	cout << "Hello World!" << endl;	// commentaire fin de ligne
}

// ctrl k + ctrl d -> indentation automatique
// ctrl k + ctrl c -> commenter la selection
// ctrl k + ctrl u -> dé-commenter la selection