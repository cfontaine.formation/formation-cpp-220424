#include <iostream>
#include "Maclasse.h"
#include "ClasseAmie.h"

// fonctionAmie peut accder  la variable d'instance val de la classe MaClasse car elle est dclare amie
void fonctionAmie(Maclasse& m) {
	m.data = 123;
}

int main()
{
	// Fonction amie
	Maclasse mc(42);
	mc.afficher();
	fonctionAmie(mc);
	mc.afficher();

	// Classe amie
	ClasseAmie ca(mc);
	ca.traitement();
	mc.afficher();
}

