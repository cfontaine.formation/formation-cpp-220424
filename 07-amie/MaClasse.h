#pragma once
class Maclasse
{
	int data;

public:
	Maclasse(int data) :data(data) {

	}

	int getData() const {
		return data;
	}

	void afficher() const;

	// Fonction Amie
	friend void fonctionAmie(Maclasse&);

	// Classe Amie
	friend class ClasseAmie;
};




