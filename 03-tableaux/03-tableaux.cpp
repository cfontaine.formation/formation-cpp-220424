#include <iostream>
#include <climits>	// include pour pouvoir utiliser INT_MIN

using namespace std;

int main()
{
	// Tableau (statique) à une dimension
	// Déclaration: type nom[taille]
	double t[5];

	// Initialisation du tableau
	for (int i = 0; i < 5; i++) {
		t[i] = 0.0;
	}

	// Accès à un élément du tableau
	t[0] = 1.23;
	cout << t[0] << endl;

	// Parcourir un tableau
	for (int i = 0; i < 5; i++) {
		cout << "t[" << i << "]" << t[i] << endl;
	}

	// Parcourir un tableau en c+11
	// uniquement en lecture -> la modification de la variable elm n' a d'influence sur le contenu du tableau
	for (auto elm : t) { // double
		cout << elm << " ";
		elm = 5.5; // elm -> uniquement en lecture si on modifie elm , on ne modifie pas le tableau
	}
	cout << endl;
	for (auto elm : t) {
		cout << elm << " ";
	}
	cout << endl;

	// Déclaration et initialisation d'un tableau
	int t2[] = { 2,6,4,9,1 };	// la taille du tableau correspond au nombre d'élément
	int t3[4] = { 6,3,7,1 };
	//int t4[3] = { 4,8,1,4 }; // -> erreur de compilation
	int t5[6] = { 3,4 };// -> 3 4 0 0 0 0
	int tz[15] = { }; // on initialise tous les élément à 0  correspond à {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
	for (auto e : tz) {
		cout << e << " ";
	}

	// Calculer la taille d'un tableau (ne fonctionne pas avec les tableaux passés en paramètre de fonction
	int size = sizeof(t) / sizeof(double); // 5
	cout << size << endl;

	// la taille du tableau doit être une constante ou une litterale
	const int S = 5;
	int t6[S];
	cout << endl;

	// /!\ pas de vérification de l'indice en c/c++
	// cout << t[20] << endl;
	// t[20] = 5.6;

	// Exercice : tableau
	// Trouver la valeur maximale et la moyenne d’un tableau de 5 entiers: -7, 4, 8, 0, -3
	int tab[] = { -7, -4, -8, -11, -3 };
	int max = INT_MIN;// tab[0];
	//int somme = 0;
	double somme = 0.0;
	for (auto e : tab) {
		if (e > max) {
			max = e;
		}
		somme += e; // somme=somme+e;
	}
	// ou double moyenne = somme / 5.0;
	// ou double moyenne = static_cast<double>(somme) / 5;
	double moyenne = somme / 5;
	cout << "maximum=" << max << " moyenne= " << moyenne << endl;

	// Tableau à 2 dimensions
	// Déclaration
	char t2d[3][4];

	// Initialisation de tous les élément avec le caractère a
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 4; j++) {
			t2d[i][j] = 'a';
		}
	}

	// Accès à un élément
	t2d[0][0] = 'z';
	cout << t2d[1][2] << endl;


	// Parcourir un tableau à 2 dimensions
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 4; j++) {
			cout << t2d[i][j] << "\t";
		}
		cout << endl;
	}

	// Calculer le nombre  de colonnne  d'un tableau (ne fonctionne pas avec les tableaux passés en paramètre)
	int nbColonne = sizeof(t2d[0]) / sizeof(char); // 4

	// Calculer le nombre  de ligne  d'un tableau
	int nbLigne = sizeof(t2d) / sizeof(t2d[0]); //3

	// Calculer le nombre  d'élément  d'un tableau
	int nbElement = sizeof(t2d) / sizeof(char); //12
	cout << nbColonne << " " << nbLigne << " " << nbElement << endl;

	// Déclaration et initialisation
	// char t2d[3][2] = { {'a','z'},{'e','r'},{'r','t'} };
	char t2d2[3][2] = { 'a','b','c','d','e','f' };
	char t2d2b[3][2] = { {'a','b'},{'c','d'},{'e','f'} };
	int t2di[5][6] = {}; // tous les éléments sont initialisés à 0

	for (int i = size - 1; i >= 0; i--) {
		cout << t[i] << " ";
	}


	// Pointeur
	// Déclaration d'un pointeur de double
	double* ptr; // pointeur -> une variable qui contient une adresse mémoire

	double v = 1.23;
	cout << v << endl;

	// &v -> adresse de la variable v
	cout << &v << endl;
	ptr = &v;
	cout << ptr << endl;

	// *ptr -> Accès au contenu de la variable pointer par ptr
	cout << ptr << " " << *ptr << endl;

	// modifier la valeur contenu dans v par l'intermédiaire du pointeur
	*ptr = 6.7;
	cout << v << endl;

	// Un pointeur qui ne pointe sur rien
	double* ptr2 = 0; // -> C++ 0 = NULL en C
	double* ptr3 = nullptr; // -> C++ 11

	// affectation de pointeur de même type
	ptr2 = ptr;
	cout << ptr << " " << *ptr << endl;
	cout << ptr2 << " " << *ptr2 << endl;

	// comparer 2 pointeurs du même type ==  ou !=
	if (ptr2 == ptr) {
		cout << "Les 2 pointeurs sont égaux" << endl;
	}

	// comparer un pointeur à 0 ou nullptr
	if (ptr3 == 0) { //nullptr
		cout << "Le pointeur ptr3 ne pointe sur rien" << endl;
	}

	// Pointeur constant
	// En préfixant un pointeur avec const -> Le contenu de la variable pointée est constant
	const double* ptrConstant = &v;
	cout << *ptrConstant << endl; // 6.7
	// *ptrConstant = 1.2;	// on ne peut plus modifier le contenu pointé par l'intermédiaire du pointeur
	ptrConstant = nullptr;	// mais on peut modifier le pointeur

	ptrConstant = &v;
	v = 12.78;

	// En postfixant un pointeur avec const -> le pointeur est constant
	double* const ptrCstPost = &v;
	cout << *ptrCstPost << endl;
	*ptrCstPost = 3.4;			// On peut modifier le contenu pointé
	//ptrCstPost = nullptr;		// Le pointeur ptrCstPost est constant, on ne peut plus le modifier

	// En préfixant et en postfixant un pointeur avec const: le pointeur ptrCC est constant et
	// l'on ne peut plus modifier le contenu de la variable pointée par l'intermédiaire du pointeur
	const double* const ptrCC = &v;
	cout << *ptrCC << endl;
	//*ptrCC = 9.0;
	//ptrCC = nullptr;


	// affecter un pointeur constant avec un pointeur => OK
	const double* ptr4 = ptr;

	// affecter un pointeur  avec un pointeur constant => erreur
	//double* ptr5 = ptrCst;


	// en C, opérateur de cast permet de supprimer le qualificatif const
	double* ptr6 = (double*)ptrConstant;

	// en C++, const_cast permet de supprimer le qualificatif const
	double* ptr7 = const_cast<double*>(ptrConstant);

	// Exercice: Pointeur
	// Créer
	// - 3 variables de type double: v1, v2, v3
	// - créer un pointeur de double : ptrChoix
	//	entrer une valeur entre 1 et 3 et  Afficher la valeur de la variable choisie par l'intermédiare du pointeur
	double v1 = 1.2;
	double v2 = 3.4;
	double v3 = 5.6;
	double* ptrchoix;
	int choix;
	cin >> choix;
	switch (choix) {
	case 1:
		ptrchoix = &v1;
		break;
	case 2:
		ptrchoix = &v2;
		break;
	default:
		ptrchoix = &v3;
		break;
	}
	cout << *ptrchoix << endl;


	// Tableau et pointeur
	// le nom du tableau est un pointeur sur le premier élément
	int tp[] = { 4,6,8,3 };
	// nom du tableau est un pointeur sur le 1er élément de tableau
	cout << tp << endl;		// pointeur sur le 1er élément du tableau
	cout << *tp << endl;	// contenu du 1er élément deutableau

	// On accède au 4ème élément du tableau
	cout << tp + 3 << endl;
	cout << *(tp + 3) << endl;

	int* ptrTab = tp;
	cout << *(ptrTab + 2) << " " << ptrTab[2] << endl;


	int* ptrTp = tp; // *ptrTp=4
	cout << *(ptrTp + 1) << " " << tp[1] << " " << ptrTp[1] << endl;
	ptrTp++; // *ptrTp= 6

	// Exercice: Pointeur (tableau)
	// idem en remplaçant les variables v1 à v3 par un tableau de double de 5 éléments
	//double td[] = { 1.2,3.4,5.6,7.8,8.9 };
	//double* ptrChoixTd = td;
	//int choixTd;
	//cin >> choixTd;
	//if (choixTd > 0 && choixTd < 6) {
	//	cout << *(ptrChoixTd + choixTd - 1) << endl;
	//	cout << ptrChoixTd[choixTd - 1] << endl;
	//}


	// Pointeur void -> C void*
	double d = 7.89;
	double* ptrD = &d;
	void* ptrVoid = ptrD;
	// cout << *ptrVoid << endl; // *void -> ne permet pas le déférencement
	int* ptrI = (int*)ptrVoid;
	cout << *ptrI << endl;

	// reinterpret_cast en c++						  64  
	int rti = 0x61 | 0x64 << 8 | 0x55 << 16;  //00 55 64 61
	cout << hex << rti << endl; // hex pour afficher en héxdécimal
	int* pRti = &rti;
	char* ptrChr = reinterpret_cast<char*>(pRti);
	cout << *ptrChr << " " << static_cast<int>(*ptrChr) << endl; // 61
	ptrChr++;
	cout << *ptrChr << " " << static_cast<int>(*ptrChr) << endl; // 64
	ptrChr++;
	cout << *ptrChr << " " << static_cast<int>(*ptrChr) << endl; // 55


	// Allocation dynamique
	int* ptrDyn = new int;		// new -> allocation dynamique 
	*ptrDyn = 42;
	cout << dec << ptrDyn << " " << *ptrDyn << endl;
	// ...
	delete ptrDyn;	// delete-> libération de la mémoire alloué		
	ptrDyn = nullptr;

	delete ptrDyn;	// Pas de problème, si on utilise delete sur un pointeur null, il ne  se passe rie

	// Allocation dynamique et intialisation en C++
	ptrDyn = new int(23);
	cout << *ptrDyn << endl;
	//
	delete ptrDyn;

	// Allocation dynamique et intialisation en C++ 11
	ptrDyn = new int{ 25 };
	cout << *ptrDyn << endl;
	//
	delete ptrDyn;

	// Allocation dynamique d'un tableau
	int s = 4;
	double* ptrTabDyn = new double[s];	// new [] => création d'un tableau dynamique
	*ptrTabDyn = 2.3;		// équivalant à ptrTabDyn[0]=2.3
	*(ptrTabDyn + 1) = 3.4;	// équivalant à ptrTabDyn[1] = 3.14
	cout << *ptrTabDyn << " " << ptrTabDyn[0] << endl;
	cout << *(ptrTabDyn + 1) << " " << ptrTabDyn[1] << endl;
	// => On peut accèder à un tableau dynamique comme à un tableau statique
	ptrTabDyn[2] = 7.5;
	ptrTabDyn[3] = 4.5;

	for (int i = 0; i < 4; i++) {
		cout << ptrTabDyn[i] << endl;
	}

	//
	delete[] ptrTabDyn;	// delete [] => libération d'un tableau dynamique
	ptrTabDyn = nullptr;

	// Exercice : Tableau dynamique
	// Modifier le programme Tableau pour faire la saisie :
	// - de la taille du tableau
	// - des éléments du tableau
	// Trouver la valeur maximale et la moyenne du tableau
	int nbElements;
	cin >> nbElements;
	int* tabDyn = nullptr; // ou 0
	if (nbElements > 0) {
		tabDyn = new int[nbElements];
		for (int i = 0; i < nbElements; i++) {
			cout << "t[" << i << "]=";
			cin >> tabDyn[i];
		}
		max = INT_MIN;
		somme = 0.0;
		for (int i = 0; i < nbElements; i++) {
			if (tabDyn[i] > max) {
				max = tabDyn[i];
			}
			somme += tabDyn[i];
		}
		cout << "maximum=" << max << " moyenne=" << (somme / nbElements) << endl;
	}
	delete[] tabDyn;



	// Référence (uniquement en C++)
	// Une référence coorrpond à un autre nom ue l'on donne à la variable
	// Pour initialiser une référence on utilse un lvalue (=qui possède une adresse) 
	int u = 12;

	// Déclarer une référence
	int& ref = u;

	cout << u << " " << ref << endl;
	ref = 23;
	cout << u << " " << ref << endl;

	// double& ref;		 // Il faut obligatoirement l'initialiser avec une variable
	//double &ref2 = 34; // Obligatoirement  une variable


	// Référence Constante
	const int& refCst = u;
	cout << refCst << endl;
	// refCst = 56; // On ne peut pas modifier la valeur d'une référence constante
	u = 56;
	cout << u << " " << ref << " " << refCst << endl;

	// une référence constante peut être initialisée avec une littérale
	const int& refCst2 = 45;

	// En utilisant une référence -> la modifcation de la variable va modifier le contenu du tableau
	int tab3[] = { 1,4,7,8 };
	for (auto& elm : tab3) {
		cout << elm << " ";
		elm = 42;
	}

	for (auto elm : tab3) {
		cout << elm << " ";
	}
}

