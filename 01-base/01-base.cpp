#include <iostream>
#include <bitset>
#define VAL_CONST 12    // Macro en C => pour définir une constante  (à éviter en C++)

using namespace std;
// Variable global -> Variable déclarée directement dans le fichier et pas dans un bloc de code
//				      (à éviter au maximum)
// Si elle n'est pas initialisée un variable globale est initialisée à 0
int vGlobal = 123;

int main()
{
	// Déclaration variable
	int i;

	// Intialisation
	i = 42;

	// Déclaration et initialisation
	double hauteur = 123.5;		// C
	double hauteurCpp98(123.5);	// C++98
	double hauteurCpp11{ 123.5 };	// C++11
	double hauteurCpp11Bis = { 123.5 };
	cout << hauteur << "  " << hauteurCpp98 << "  " << hauteurCpp11 << "  " << hauteurCpp11Bis << endl;

	// Déclaration multiple
	int mb, mc = 10;
	mb = 23;
	cout << mb << " " << mc << endl;

	// Littéral entier  -> par défaut int
	long l = 10L;			// Litéral long -> L
	unsigned int ui = 10U;	// Littéral unsigned -> U
	long long lli = 10LL;	// Littéral long long -> LL en C++ 11

	// Changement de base
	int decimal = 42;	// base 10 par défaut: décimal
	int hexa = 0xff32;	// base 16 héxadécimal -> 0x
	int octal = 0342;	// base 8  octal -> 0
	int bin = 0b1100110;// base 2  binaire -> 0b en C++14
	cout << decimal << " " << hexa << " " << octal << " " << bin << endl;

	// Littéral virgule flottante
	double dd = 1.5;		// 5.-> OK, .78 -> OK
	double dEx = 1.234e3;
	// Littéral virgule flottante -> par défaut de type double
	float f = 1.5F;			// littéral float -> F
	long double ld = 1.5L;	// Littéral long double -> L

	// Littéral booléen
	bool test = true; // ou false

	// Littéral caractère
	char c = 'a';
	char cHexa = '\x41';	// caractère en hexadécimal
	char cOctal = '\32';	// caractère en octal
	char cAscii = 97;

	// Variable globale
	cout << "Variable globale" << vGlobal << endl; // 123

	// On déclare une variable locale qui a le même le nom que la variable globale
	int vGlobal = 12;
	// La variable globale est masquée par la variable locale
	cout << "Variable locale" << vGlobal << endl; //12
	// Pour accéder à la variable globale, on utilise l’opérateur de résolution de portée ::
	cout << "Variable globale" << ::vGlobal << endl; //0

	// En C pour une constante, on utilise #define 
	int v = VAL_CONST;
	cout << v << endl;

	// En C++ -> const
	// constante ->  const
	const double PI = 3.14;
	// const double PI;	// Erreur: on est obligé de faire l'initialisation
	// PI = 3.1419;		// Erreur: on ne peut pas modifier une constante

	// en c++ 11 -> constexpr 
	constexpr double ct = PI * 2; // constant -> le calcul a lieu à la compilation
	// int tn = 34;
	// constexpr double ct2 = tn * 2; // Erreur -> tn n'est pas constant et ne peut pas être définie à la compilation

	// Typage implicite C++ 11 -> auto
	// Le type de la variable est définie à partir de l'expression d'initialisation
	auto implicite1 = 3.67f; // implicite de type float
	auto implicite2 = false; // implicite de type bool
	// Attention aux chaine de caratères: avec auto le type sera const char* et pas std::string
	auto implicte3 = "azerty"; // const char *
	auto implicte4 = PI; // auto ne tient pas compte de const et constexpr
	implicte4 = 5.6; // n'est constant, on peut le modifier

	// decltype  C++ 11 -> déclarer qu'une variable est de même type qu'une autre
	decltype(c) c2 = 'Z';	// c2 est de type char
	decltype(PI) PI3 = 3.14; // const double
	decltype(PI * 5 + 12.5) PI4; //double

	// Opérateurs
	// Opérateur arithmétique
	int aa = 1;
	int bb = 3;
	int res = aa + bb;
	cout << res << " " << bb % 2 << endl;	// % => modulo (reste de division entière) uniquement avec des entiers positif

	// Division par 0
	// Uniquement avec les nombres à virgule flottante -> inf -inf et nan
	double d1 = 1.0;
	double d2 = 0.0;
	cout << " 1.0/0.0=" << d1 / d2 << " -1.0/0.0=" << -d1 / d2 << " 0.0/0.0 =" << d2 / d2 << endl;

	// une division par 0 avec un entier -> exception
	int i1 = 1, i2 = 0;
	// cout << i1 / i2 << endl;

	// Pré-incrémentation
	int inc = 0;
	int res = ++inc; // inc =1, res=1
	cout << inc << " " << res;

	// Post-incrémentation
	inc = 0;
	res = inc++; // res =0 ; inc=1
	cout << inc << " " << res;

	// Affectation composée
	res = 12;
	res += 30; // correspond à res=res+30

	// Opérateur de comparaison -> le résultat est un booléen
	int val = 3;
	bool tst1 = val < 100; // true

	// Opérateur logique
	//  non !
	bool inv = !tst1; //false

	// T1 T2 | ET  OU Ou exclusif
	// F  F  | F   F	F
	// F  T  | F   T	T
	// T  F  | F   T	T
	// T  T  | T   T	F


	// Opérateur court-circuit && -> et  et || -> ou (évaluation garantie de gauche à droite)
	// && dés qu'une comparaison est fausse, les suivantes ne sont pas évaluées
	val = 3;
	bool tst2 = val > 100 && val++ < 10;	// si val > 100 est faux,  val++ < 10 n'est pas évalué
	cout << tst2 << " " << val << endl;

	// || dés qu'une comparaison est vrai, les suivantes ne sont pas évaluées
	bool tst3 = val == 3 || val++ > 100;
	cout << tst3 << " " << val << endl;		// si val == 3  est vrai,  val++ > 100 n'est pas évalué

	// Opérateur binaire (uniquement avec des entiers)
	// Pour les opérations sur des binaires, on peut utiliser les entiers non signé
	unsigned int b1 = 0b10011;
	cout << ~b1 << " " << bitset<32>(~b1) << endl;	// Complémént: 1 -> 0 et 0 -> 1
	cout << bitset<32>(b1 & 0b1001) << endl;		// Et bit à bit 0001
	cout << bitset<32>(b1 | 0b1001) << endl;		// Ou bit à bit 11011
	cout << bitset<32>(b1 ^ 0b1001) << endl;		// Ou exclusif bit à bit 11010

	// Opérateur de décallage
	cout << bitset<32>(b1 >> 2) << endl; // 100		Décalage à droite de 2
	cout << bitset<32>(b1 << 1) << endl; // 100110	Décalage à gauche de 1 (-> équivaut à une multiplication par 2)
	cout << bitset<32>(b1 >> 1) << endl; // 1001	Décalage à droite de 1 (-> équivaut à une division par 2)

	// Affection composé avec les opérateurs binaire
	b1 &= 0b1001;	// b1= b1 &1001
	cout << bitset<32>(b1) << endl; //b1=0001
	b1 <<= 3;		// b1= b1 <<3	
	cout << bitset<32>(b1) << endl; // b1=1000

	// sizeof
	cout << sizeof(int) << endl;		// nombre d'octets du type int -> 4
	cout << sizeof(hauteur) << endl; // nombre d'octets de la variable hauteur -> 8

	// Opérateur séquentiel, toujours évalué de gauche->droite
	inc = 0;
	int o = 12;
	res = (inc++, o + 10);	// res=o+10; inc=inc+1
	cout << res << endl;	//22

	// Conversion implicite -> rang inférieur vers un rang supérieur (pas de perte de donnée)
	int tii = 1;
	double tid = 4.5;
	auto r3 = tii + tid; // double
	cout << r3 << endl;

	// Promotion numérique -> short, bool ou char dans une expression -> convertie automatiquement en int
	short s1 = 2;
	short s2 = 3;
	auto s3 = s1 + s2; // int

	// boolean vers entier
	int convI1 = false; // false ->  0
	int convI2 = true;	// true -> 1

	// entier,nombre à virgule flottante et pointeur -> boolean
	bool cb1 = 34;	// 34 différent de 0 -> true
	bool cb2 = 0;	// égale à 0 -> false
	int* ptr = nullptr;
	bool cb3 = ptr; // pointeur = à null -> false
	bool cb4 = &convI1; // pointeur différent de null -> true
	cout << cb1 << " " << cb2 << " " << cb3 << " " << cb4 << endl;

	// Conversion explicite -> opérateur de cast
	int te = 11;
	double ted = te / 2; // 5.0
	cout << ted << endl;

	double ted2 = te / 2.0; // 5.5
	cout << ted << endl;

	// En C opérateur de cast (type) 
	int d = 2;
	double te3 = ((double)te) / d; //5.5
	cout << te3 << endl;

	// En C++ static_cast<type>(variable)
	// avec static_cast, le compilateur fait plus de vérification (sur le type)
	double te4 = static_cast<double>(te) / d;
	cout << te4 << endl;

	// Opérateur d'affectation conversion systémathique (implicite ou explicite)
	// conversion explicite: double -> int
	double ta = 3.5;
	int convExp = ta;
	cout << convExp << endl;	// 3

	// conversion implicite: int->double
	int ta2 = 4;
	double convImp = ta2;
	cout << convImp << endl;	//4.0


	// Exercice: Moyenne
	// Saisir 2 nombres entiers et afficher la moyenne dans la console
	int v1, v2;
	cout << "Saisir 2 nombres entiers: ";

	int v1, v2;
	cin >> v1 >> v2;
	double moyenne = (v1 + v2) / 2.0;
	// double moyenne = static_cast<double>(v1 + v2) / 2.0;
	//double moyenne = ((double) (v1 + v2)) / 2;
	cout << "Moyenne=" << moyenne << endl;
}
