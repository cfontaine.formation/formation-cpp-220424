#include <iostream>
#include "ChaineCaractere.h"
#include "Flux.h"
#include "Container.h"

int main()
{
	// Chaine de caractère
	exempleString();
	
	// Exercice inverser
	cout << inverser("bonjour") << endl;

	// Exercice palindrome
	cout << palindrome("bonjour") << endl;
	cout << palindrome("sos") << endl;

	// Exercice: accronyme
	cout << acronyme("Comité international olympique") << endl;
	cout << acronyme("Organisation du traité de l'Atlantique Nord") << endl;

	// Flux
	ecrireText("c:\\Dawan\\Formations\\test.txt");
	lireText("c:\\Dawan\\Formations\\test.txt");

	// Container
	exempleContainer();
}
