#pragma once
#include <string>

using namespace std;

void exempleString();
std::string inverser(const std::string&);
bool palindrome(const std::string&);
std::string acronyme(const std::string& str, int size=2);