#include "ChaineCaractere.h"
#include <iostream>


using namespace std;

void exempleString() {
	// Chaine de caract�re en C++ -> string
	string strCpp = "Hello";
	cout << strCpp << endl;
	string str1 = string("bonjour");
	string str2 = string(15, 'a');
	cout << str1 << " " << str2 << endl;

	// Allouer dynamiquement 
	string* str3 = new string("azerty");
	cout << *str3 << endl;
	delete str3;

	// Concat�nation
	strCpp += " World";			// Op�rateur + -> 	// Concat�nation
	cout << strCpp << endl;
	strCpp.append(" !!!");		// append -> permet d'ajouter la chaine pass�e en param�tre en fin de chaine
	cout << strCpp << endl;
	strCpp.push_back('?');		// push_back -> ajoute le caract�re pass� en param�tre en fin de chaine
	cout << strCpp << endl;

	// Comparaison
	// On peut utiliser les op�rateurs de comparaison avec les chaines de caract�res
	if (str1 == str2) {
		cout << "==" << endl;
	}
	else {
		cout << "!=" << endl;
	}

	if (str2 < str1) {
		cout << "Inferieur" << endl;
	}

	// Acc�s � un cararact�re de la chaine [] ou at
	cout << strCpp[1] << endl;
	cout << strCpp.at(1) << endl;

	// Si l'indice d�passe la taille de la chaine
	//	cout << strCpp[40] << endl;		// [] -> erreur du programme
	//	cout << strCpp.at(40) << endl;	// at -> une exception est lanc�

	// length -> nombre de caract�res de la chaine (idem pour size)
	cout << strCpp.length() << endl;

	// empty -> retourne true si la chaine est vide
	cout << strCpp.empty() << endl;

	// Extraire une sous-chaine -> substr
	cout << strCpp.substr(5) << endl;		// retourne une  sous-chaine qui commence � l'indice 5 et jusqu'� la fin
	cout << strCpp.substr(6, 5) << endl;	// retourne une  sous-chaine de 5 caract�res qui commence � l'indice 6

	// Ins�rer une sous-chaine -> insert
	strCpp.insert(5, "-----");	// ins�re la chaine pass�e en param�tre � la position 5
	cout << strCpp << endl;

	// remplacer une partie de la chaine par une sous-chaine -> replace
	strCpp.replace(6, 1, "_?!");  // remplace � la position 6 , 1 caract�re par la chaine de caract�re pass� en param�tre par _?!
	cout << strCpp << endl;

	// Supprimer une partie de la chaine -> erase
	strCpp.erase(5, 7);		// supprimer 7 caract�res de la chaine � partir de la position 5
	cout << strCpp << endl;
	strCpp.erase(12);		// supprimer les caract�res de la chaine � partir de la position 12 jusqu'� la fin de la chaine
	cout << strCpp << endl;

	// Recherche de la position de la chaine ou du caract�re pass�e en param�tre dans la chaine str
	// -> find
	cout << strCpp.find('o') << endl;		// � partir du d�but de la chaine
	cout << strCpp.find('o', 5) << endl;		// � partir de la position 5
	// � partir de la position 8, pas de caract�re => retourne la valeur std::string::npos
	cout << strCpp.find('o', 8) << " " << string::npos << endl;

	// -> rfind
	// idem find, mais on commence � partir de la fin de la chaine et on va vers le debut
	cout << strCpp.rfind('o') << endl;


	// Iterateur = > objet qui permet de parcourir la chaine(un peu comme un pointeur)
	// begin => retourne un iterateur sur le d�but de la chaine
	// end => retourne un iterateur sur la fin de la chaine
	for (string::iterator it = strCpp.begin(); it != strCpp.end(); it++) {	  // it++ permet de passer au caract�re suivant
		cout << *it << endl;	// *it permet d'obtenir le caract�re "pointer" par l'it�rateur
		//	*it = 'a';			// on a acc�s en lecture et en �criture
	}
	cout << strCpp << endl;

	// rbegin et rend idem  mais l'it�rateur part de la fin et va vers le d�but de la chaine
	for (auto it = strCpp.rbegin(); it != strCpp.rend(); it++) {	// it++ permet de passer au caract�re pr�c�dent
		cout << *it << endl;
	}

	// en C++11 
	// Parcourir une chaine compl�tement
	for (auto chr : strCpp) {
		cout << chr << endl;
	}

	// Convertion string en chaine C (const *char)	-> c_str
	cout << strCpp.c_str() << endl;

	// Efface les caract�res contenus dans la chaine
	strCpp.clear();

	// empty => retourne true si la chaine est vide
	cout << strCpp.empty() << endl;
}


// Exercice Inversion de chaine
// �crire la fonction inverser qui prend en param�tre une chaine et qui retourne la chaine avec les caract�res invers�s
std::string inverser(const std::string& str)
{
	string tmp = "";
	for (auto it = str.rbegin(); it != str.rend(); it++) {
		tmp.push_back(*it);
	}
	return tmp;
}

// Exercice Palindrome
// �crire une m�thode Palindrome qui accepte en param�tre une chaine et qui retourne un bool�en pour indiquer si c'est palindrome
bool palindrome(const std::string& str)
{
	return str == inverser(str);
}

// Acronyme
// Faire une m�thode Acronyme qui prend en param�tre une phrase et qui retourne un acronyme
// Les mots de la phrase sont pris en compte s'ils contiennent plus de n lettres, n est un param�tre de la fonction (valeur par d�faut 2)
std::string acronyme(const std::string& str, int size)
{
	string acr = "";
	auto itDebutMot = str.begin();
	for (auto it = str.begin(); it != str.end(); it++)
	{
		if (*it == ' ' || *it == '\'' || it == str.end() - 1)
		{
			if (it - itDebutMot > size)
			{
				acr.push_back(toupper(*itDebutMot)); // toupper pour convertir un caract�re minuscule -> majuscule
			}
			itDebutMot = it + 1;
		}
	}
	return acr;
}