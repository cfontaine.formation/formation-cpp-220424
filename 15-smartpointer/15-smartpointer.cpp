#include <iostream>
#include <memory>
#include"Point.h"

using namespace std;


int main()
{
	// Pointeurs intelligents
	unique_ptr<int> uniPtr; // nullptr

	unique_ptr<int> uniPtr1(new int(12)); // {new int(12)}
	cout << *uniPtr1 << " " << uniPtr1.get() << endl;

	unique_ptr<int> uniPtr2{ new int(25) };
	cout << *uniPtr2 << " " << uniPtr2.get() << endl;
	//uniPtr2=uniPtr1;
	uniPtr2 = std::move(uniPtr1); // uniPtr1 nullptr 
	cout << *uniPtr2 << " " << uniPtr2.get() << endl;

	// C++14
	unique_ptr<int> uniPtr3 = std::make_unique<int>(1);

	//shared_ptr<int> shrptr1(new int(23));
	shared_ptr<int> shrptr1;
	shrptr1 = move(uniPtr2);
	shared_ptr<int> shrptr2(shrptr1);
	shared_ptr<int> shrptr3;
	shrptr3 = shrptr1;
	cout << *shrptr1 << " " << shrptr1.get() << "  " << shrptr1.use_count() << endl;
	shrptr3 = nullptr;
	cout << *shrptr1 << " " << shrptr1.get() << "  " << shrptr1.use_count() << endl;

	unique_ptr<Point> uvPtr(new Point(1,2));
	unique_ptr<Point> uvPtr2 = make_unique<Point>(3,4);
	uvPtr->afficher();
	unique_ptr<Point> uvPtr3 = move(uvPtr2);
	uvPtr3->afficher();

	shared_ptr<Point> svPtr(new Point(5,6));
	shared_ptr<Point> svPtr1(svPtr);
	shared_ptr<Point> svPtr2(make_shared<Point>(7,8));

	svPtr->afficher();
	svPtr1->afficher();

	svPtr2->afficher();
	cout << svPtr.use_count() << " " << svPtr2.use_count() << endl;

	svPtr2 = move(uvPtr3);
	svPtr2->afficher();
	cout << "---------------------" << endl;

}
