#include <iostream>
#include <string>
#include "Pair.h"

#include<type_traits>


using namespace std;

template<typename T> T somme(T a, T b)
{
   return a + b;
}
// is_integral -> entier, is_floating -> virgule flotante, is_base_of _v<classeDeBase,T> -> une classe mère et ses sous classe
template<typename T ,typename U, typename = std::enable_if_t<is_integral<U>::value>> U somme2(T a, U b)
{
    return a + b;
}

int main()
{
    // Fonction Générique
    cout << somme<int>(3, 5) << endl;
    cout << somme(3, 5) << endl;
    cout << somme(1L, 5L) << endl;
    cout << somme(0.6, 1.4) << endl;
    cout << somme(string("aze"), string("rty"));

    cout << somme2(2.5, 1) << endl;
    cout << somme2(2.5, 1L) << endl;
   // cout << somme2(2.5, 1.2) << endl; // 1.2 n'est pas un type entier

    // Classe générique
    Pair<int> p1(12, 34);
    cout << p1.maximum()<<endl;

    Pair<string> p2("aze", "rty");
    cout << p2.minimum() << endl;
}
