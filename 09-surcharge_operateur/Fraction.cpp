#include "Fraction.h"
#include <ostream>
#include <string>

Fraction operator+(const Fraction& f1, const Fraction& f2)
{
	int n = f1.numerateur * f2.denominateur + f2.numerateur * f1.denominateur;
	int d = f1.denominateur * f2.denominateur;
	int p = Fraction::pgcd(n,d);
	return Fraction(n/p, d/p);
}

Fraction operator*(const Fraction& f1, const Fraction& f2)
{
	int n = f1.numerateur * f2.numerateur;
	int d = f1.denominateur * f2.denominateur;
	int p = Fraction::pgcd(n, d);
	return Fraction(n / p, d / p);
}

Fraction operator*(const Fraction& f1, const int& scal)
{
	int n = f1.numerateur * scal;
	int d = f1.denominateur;
	int p = Fraction::pgcd(n, d);
	return Fraction(n / p, d / p);
}

bool operator==(const Fraction& f1, const Fraction& f2)
{
	return f1.valeur() > f2.valeur() - 0.000000000000001 && f1.valeur()< f2.valeur() + 0.000000000000001;
}

bool operator!=(const Fraction& f1, const Fraction& f2)
{
	return !(f1 == f2);
}

std::ostream& operator<<(std::ostream& os, const Fraction& f)
{

	return os << f.numerateur << (f.denominateur == 1 ? "" : "/" + std::to_string(f.denominateur));
}

int Fraction::pgcd(int a, int b)
{
	while (a != b) {
		if (a > b) {
			a -= b;
		}
		else {
			b -= a;
		}
	}
	return a;
}
