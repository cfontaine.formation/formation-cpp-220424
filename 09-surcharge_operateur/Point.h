#pragma once
#include <ostream>
class Point
{
	int x;
	int y;

public:
	Point(int x = 0, int y = 0) :x(x),y(y) {

	}

	double getX() const {
		return x;
	}

	double getY() const {
		return y;
	}

	void setX(int x) {
		this->x = x;
	}

	void setY(int y) {
		this->y = y;
	}

	void afficher() const;
	void deplacer(int tx, int ty);
	double norme() const;

	double distance(Point p1);

	static double distance(Point& p1, Point& p2);

	// surcharge d'op�rateur ->le mot-clef operator suivie de l'op�rateur � surcharger

	// Op�rateur unaire -> m�thode d'instance
	Point operator-();
	Point operator++();		// pr�-incr�mentation
	Point operator++(int);	// post-incr�mentation, int sert uniquement � indiquer la post-incr�mentation
	int& operator[](int index);

	// Op�rateur binaire -> on va utiliser des fonctions amies
	friend Point operator+(const Point& p1, const Point& p2);
	friend Point operator*(const Point& p1, const int& m); // au moins un des 2 param�tre doit �tre de type Point
	friend Point operator*(const Point& p1, const Point& p2);
	friend bool operator==(const Point& p1, const Point& p2);
	friend bool operator!=(const Point& p1, const Point& p2);
	friend std::ostream& operator<<(std::ostream& os, Point p);
};

