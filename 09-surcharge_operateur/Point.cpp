#include "Point.h"
#include <iostream>
#include <cmath>

using namespace std;

void Point::afficher() const
{
	cout << "(" <<x<<","<<y<< ")" << endl;
}

void Point::deplacer(int tx, int ty)
{
	x += tx;
	y += ty;
}

double Point::norme() const
{
	return std::sqrt(x * x + y * y);
}

double Point::distance(Point p1)
{
	return std::sqrt(std::pow((x - p1.x), 2) + std::pow((y - p1.y), 2));
}

double Point::distance(Point& p1, Point& p2)
{
	return std::sqrt(std::pow((p2.x - p1.x), 2) + std::pow((p2.y - p1.y), 2));
}

Point Point::operator-()
{
	return Point(-x,-y);
}

Point Point::operator++()
{
	x++;
	y++;
	return Point(x,y);
}

Point Point::operator++(int)
{
	Point tmp = *this;
	x++;
	y++;
	return tmp;
}

int& Point::operator[](int index)
{
	switch (index) {
	case 0:
		return x;
	case 1:
		return y;
	default:
		throw std::out_of_range("indice sup�rieur � 1 ou inf�rieur � 0");
	}
}

Point operator+(const Point& p1, const Point& p2)
{
	return Point(p1.x+p2.x,p1.y+p2.y);
}

Point operator*(const Point& p1, const int& m)
{
	return Point(p1.x*m,p1.y*m);
}

Point operator*(const Point& p1, const Point& p2)
{
	return Point(p1.x*p2.x),(p1.y*p2.y);
}

bool operator==(const Point& p1, const Point& p2)
{
	return p1.x == p2.x && p1.y == p2.y;
}

bool operator!=(const Point& p1, const Point& p2)
{
	return !(p1 == p2);
	// return p1.x != p2.x || p1.y != p2.y;
}

std::ostream& operator<<(std::ostream& os, Point p)
{
	return os << "(" << p.x << "," << p.y << ")";
}
