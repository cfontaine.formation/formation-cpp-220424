#include <iostream>
#include "Point.h"
#include "Fraction.h"

using namespace std;

int main()
{
    Point a(1, 2);

    a.afficher();
    // op�rateur - unaire
    Point b = -a;
    b.afficher();

    // op�rateur pr�-incr�m�ntation
    Point c = ++a;
    c.afficher();

    // op�rateur post-incr�m�ntation
    Point d = a++;
    d.afficher();
    a.afficher();

    // op�rateur []
    std::cout << a[0] << " " << a[1] << std::endl;

    // op�rateur binaire +
    Point s = a + c;
    s.afficher();

    // op�rateur binaire *
    Point m = a * 5;
    m.afficher();
    Point ia = c * b;
    Point ib = a + ia;
    Point i = a + c * b;
    ib.afficher();
    i.afficher();

    // op�rateur de comparaison
    Point e(-1, -2);
    std::cout << (b == e) << std::endl;
    std::cout << (a == b) << std::endl;
    std::cout << (b != e) << std::endl;
    std::cout << (a != b) << std::endl;

    // op�rateur <<
    std::cout << a << std::endl;

    // Exercice Fraction
    Fraction f1(4, 8);
    Fraction f2(1, 2);
    Fraction r = f1 + f2;
    std::cout << r << std::endl;
    r = f1 * f2;
    std::cout << r << std::endl;
    r = f1 * 2;
    std::cout << r << std::endl;
    cout << (f1 == f2) << endl;
    cout << (f1 != f2) << endl;

}
