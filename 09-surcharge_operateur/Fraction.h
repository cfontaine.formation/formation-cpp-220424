#pragma once
#pragma once
#include <stdexcept>

class Fraction
{
	int numerateur;
	int denominateur;

public:
	Fraction(const int& numerateur = 0, const int& denominateur = 1) : numerateur(numerateur), denominateur(denominateur) {
		if (denominateur==0) { 
			throw std::invalid_argument("d�nominateur = � 0");
		}
	}

	double valeur() const
	{
		return static_cast<double>(numerateur) / denominateur;
	}


	static int pgcd(int, int);

public:
	friend Fraction operator+(const Fraction& f1, const Fraction& f2);
	friend Fraction operator*(const Fraction& f1, const Fraction& f2);
	friend Fraction operator*(const Fraction& f1, const int& scal);
	friend bool operator==(const Fraction& f1, const Fraction& f2);
	friend bool operator!=(const Fraction& f1, const Fraction& f2);
	friend std::ostream& operator<<(std::ostream& os, const Fraction& f);
};

