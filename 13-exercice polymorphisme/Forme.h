#pragma once
enum class Couleur { VERT, ROUGE, BLEU, ORANGE };

class Forme
{
	Couleur couleur;

public:

	Forme(const Couleur& couleur) : couleur(couleur) {}

	Couleur getCouleur() const
	{
		return couleur;
	}

	virtual double calculSurface() = 0;
};

