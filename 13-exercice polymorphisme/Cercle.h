#pragma once
#include "Forme.h"
#include "Forme.h"
class Cercle : public Forme
{
	double rayon;

public:

	Cercle(const Couleur& couleur, const double& rayon) : Forme(couleur), rayon(rayon) {}

	double calculSurface() override;
};
