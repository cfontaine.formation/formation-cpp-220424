#include "Cercle.h"
#define _USE_MATH_DEFINES
#include <cmath>

double Cercle::calculSurface()
{
	return std::pow(rayon, 2) * M_PI;
}

