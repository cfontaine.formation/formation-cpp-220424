#pragma once
#include "Rectangle.h"
class TriangleRectangle : public Rectangle
{
public:
	TriangleRectangle(const Couleur& couleur, const double& largeur, const double& longueur) :Rectangle(couleur, largeur, longueur) { }

	double calculSurface() override;
};

