#pragma once
#include "Forme.h"
class Rectangle : public Forme
{
	double largeur;
	double longueur;

public:
	Rectangle(const Couleur& couleur, const double& largeur, const double& longueur) :Forme(couleur), largeur(largeur), longueur(longueur) { }

	double calculSurface() override;
};
