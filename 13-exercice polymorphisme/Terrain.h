#pragma once
#include "Forme.h"

class Terrain
{

	Forme** formes = new Forme * [10];
	int nombreForme = 0;

public:
	Terrain()
	{
		for (int i = 0; i < 10; i++) {
			formes[i] = nullptr;
		}
	}

	~Terrain() {
		delete[] formes;
	}

	bool ajoutForme(Forme& forme);

	double surfaceTotal();

	double surfaceTotal(const Couleur& couleur);

};


