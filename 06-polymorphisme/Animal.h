#pragma once
class Animal
{
	int poid;

	int age;

public:
	Animal(int poid, int age) :poid(poid), age(age) {

	}

	~Animal();

	int getAge() const {
		return age;
	}

	int getPoid() const {
		return poid;
	}

	void setAge(int age) {
		this->age = age;
	}

	void setPoid(int poid) {
		this->poid = poid;
	}

	virtual void afficher() const;

	// M�thode virtuelle 
	// une m�thode virtuelle est destin�e � �tre red�finie dans les classes qui en h�ritent
	// -> seul les constructeurs et les m�thodes statiques ne peuvent pas �tre virtuels
	// virtual void emetreSon();

	// M�thode virtuelle pure
	virtual void emetreSon() = 0; 
	
	// -> la classe devient abstraite et on ne peut plus l'instancier
	 
	// si on veut cr�er, une classe abstraite sans avoir de m�thode virtuelle pure dans la classe -> on en cr�e une avec le destructeur
	// virtual ~Animal() = 0;
};

