#pragma once
#include "Animal.h"
#include <string>
class Chien : public Animal
{
	std::string nom;

public:
	Chien(int poid, int age, std::string nom) : Animal(poid, age) {
		this->nom = nom;
	}

	std::string getNom() const {
		return nom;
	}

	void setNom(std::string nom) {
		this->nom = nom;
	}

	void afficher() const;

	// la m�thode virtuel pure doit �tre d�fini dans les classes enfants
	void emetreSon();

};

