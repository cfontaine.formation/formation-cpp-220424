#pragma once
#include "Animal.h"

class Animalerie
{
	Animal* box[10];
	int nbBoxOccupe;

public:
	Animalerie();
	~Animalerie();
	bool ajouter(Animal* animal);
	Animal* vendre(int numBox);
	void ecouter();
};


