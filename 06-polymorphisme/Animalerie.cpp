#include "Animalerie.h"
#include <iostream>

using namespace std;

Animalerie::Animalerie()
{
	for (int i = 0; i < 10; i++) {
		box[i] = nullptr;
	}
	nbBoxOccupe = 0;
}

Animalerie::~Animalerie()
{
	for (int i = 0; i < 10; i++) {
		delete box[i];
	}
}

bool Animalerie::ajouter(Animal* animal)
{
	if (nbBoxOccupe < 10) {
		for (int i=0; i < 10; i++) {
			if (box[i] == nullptr) {
				box[i] = animal;
				nbBoxOccupe++;
				return true;
			}
		}
	}
	return false;
}


Animal* Animalerie::vendre(int numBox)
{
	Animal* a = nullptr;
	if (box[numBox] != nullptr) {
		nbBoxOccupe--;
		a = box[numBox];
		box[numBox] = nullptr;
	}
	return a;
}

void Animalerie::ecouter()
{
	cout << "--------------------" << endl;
	for (int i = 0; i < 10; i++) {
		if (box[i] != nullptr) {
			cout << "   ";
			box[i]->emetreSon();
		}
		else {
			cout << "   vide" << endl;
		}
	}
	cout << "---------------------" << endl;
}
