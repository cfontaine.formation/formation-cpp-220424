#include "Chat.h"
#include <iostream>

using namespace std;

Chat::Chat(int poid, int age, int nbVie) :Animal(poid, age) {
	this->nbVie = nbVie;
}

void Chat::afficher() const
{
	Animal::afficher();
	cout << " " << nbVie << " vies"<<endl;
}

void Chat::emetreSon()
{
	cout << "Le chat miaule" << endl;
}
