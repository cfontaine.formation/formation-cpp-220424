#include "Chien.h"
#include <iostream>

using namespace std;

void Chien::afficher() const
{
	cout << "[" << nom << "] ";
	Animal::afficher();
	cout << endl;
}

void Chien::emetreSon()
{
	cout << nom << " aboie" << endl;
}
