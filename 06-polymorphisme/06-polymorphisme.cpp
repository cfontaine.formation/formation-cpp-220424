#include <iostream>
#include "Animal.h"
#include "Chien.h"
#include "Chat.h"
#include "Animalerie.h"

using namespace std;
int main()
{
	// La classe Animal est abstraite, elle ne peut plus �tre instensier
	// Animal* a1 = new Animal(3000, 4);
	// a1->afficher();
	// a1->emetreSon();

	Chien* ch1 = new Chien(7000, 8, "Rolo");
	ch1->emetreSon();
	ch1->afficher();
	Chat* chat1 = new Chat(4000, 7, 9);

	// polymorphisme
	// on acc�de a un objet par l'interm�diaire d'un pointeur de la classe m�re
	// pour une m�thode red�finie (virtual), c'est la m�thode de l'objet (classe enfant) qui est appel�

	// upcasting: object enfant ->  object parent (implicite)
	Animal* a2=new Chien(3000, 4, "Idefix");
	a2->emetreSon(); // virtual ->appel de la m�thode emmetreSon de Chien
	a2->afficher();
	
	// downcasting: object enfant <-  object parent (cast)
	// Chien* ch2 = (Chien*)a2; // cast C
	// ou
	Chien* ch2 = dynamic_cast<Chien*>(a2);
	cout << ch2->getNom() << endl;

	//Chien* ch3 = dynamic_cast<Chien*>(a1); // retourne 0 ou nulptr si le cast n'est pas possible 
	//if (ch3 != nullptr) {
	//	cout << ch3->getNom() << endl;
	//}

	// La classe Animalerie ne connait que la classe m�re Animal
	// Si on cr�e de nouvelle classe qui h�rite d'Animal, on a pas besoin de modifier la classe Animalerie  
	Animalerie* an = new Animalerie();
	an->ajouter(ch1);
	an->ajouter(a2);
	an->ajouter(chat1);
	an->ecouter();

	Animal* v1 = an->vendre(0);
	v1->afficher();
	delete v1;

	an->ajouter(new Chien(4000,8,"Laika"));
	an->ajouter(new Chien(5000, 5, "Scooby"));
	an->ajouter(new Chat(6000, 9, 3));
	an->ecouter();
	delete an;
}
